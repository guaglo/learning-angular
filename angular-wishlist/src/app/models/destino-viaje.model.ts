export class DestinoViaje{
    private selected: boolean;
    servicios: string[];
    public id: string;

    constructor(public nombre:string, public u:string){
        this.servicios = ['Desayuno Americano', 'SPA', 'Room Service 24/7'];
    }
    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected= s;

    }
}
